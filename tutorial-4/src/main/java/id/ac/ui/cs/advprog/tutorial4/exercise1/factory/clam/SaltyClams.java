package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SaltyClams implements Clams {

    @Override
    public String toString() {
        return "Salty Clams from Java Sea";
    }

}
