package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.GrilledDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;

public class DoughTest {

    private GrilledDough grilledDough;
    private ThinCrustDough thinCrustDough;
    private ThickCrustDough thickCrustDough;

    @Before
    public void setUp() {
        grilledDough = new GrilledDough();
        thinCrustDough = new ThinCrustDough();
        thickCrustDough = new ThickCrustDough();
    }

    @Test
    public void doughTest() {
        assertEquals(grilledDough.toString(), "Grilled Dough");
        assertEquals(thinCrustDough.toString(), "Thin Crust Dough");
        assertEquals(thickCrustDough.toString(), "ThickCrust style extra thick crust dough");
    }

}
