package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BaladoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Before;
import org.junit.Test;

public class SauceTest {

    private BaladoSauce baladoSauce;
    private MarinaraSauce marinaraSauce;
    private PlumTomatoSauce plumTomatoSauce;

    @Before
    public void setUp() {
        baladoSauce = new BaladoSauce();
        marinaraSauce = new MarinaraSauce();
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void sauceTest() {
        assertEquals(baladoSauce.toString(), "Balado Chili Sauce");
        assertEquals(marinaraSauce.toString(), "Marinara Sauce");
        assertEquals(plumTomatoSauce.toString(), "Tomato sauce with plum tomatoes");
    }

}
