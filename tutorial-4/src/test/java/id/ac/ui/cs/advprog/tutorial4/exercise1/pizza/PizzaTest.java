package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public class PizzaTest {

    private Pizza pizza;

    @Before
    public void setUp() {
        pizza = new Pizza() {
            @Override
            public void prepare() {
                name = "Test Pizza";
                dough = new ThinCrustDough();
                cheese = new MozzarellaCheese();
                veggies = new Veggies[]{new Onion(), new RedPepper()};
                sauce = new MarinaraSauce();
                clam = new FreshClams();
            }
        };
        pizza.prepare();
    }

    @Test
    public void pizzaTest() {
        String tested = pizza.toString();
        assertTrue(tested.contains("Test Pizza"));
        assertTrue(tested.contains(new ThinCrustDough().toString()));
        assertTrue(tested.contains(new MozzarellaCheese().toString()));
        Arrays.stream(new Veggies[]{new Onion(), new RedPepper()})
                .forEach(veggies -> assertTrue(tested.contains(veggies.toString())));
        assertTrue(tested.contains(new MarinaraSauce().toString()));
        assertTrue(tested.contains(new FreshClams().toString()));
    }

}
