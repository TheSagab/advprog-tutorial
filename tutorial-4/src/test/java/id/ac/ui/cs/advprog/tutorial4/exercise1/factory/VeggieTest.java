package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Asparagus;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import org.junit.Before;
import org.junit.Test;

public class VeggieTest {

    private Asparagus asparagus;
    private BlackOlives blackOlives;
    private Eggplant eggplant;
    private Garlic garlic;
    private Mushroom mushroom;
    private Onion onion;
    private RedPepper redPepper;
    private Spinach spinach;

    @Before
    public void setUp() {
        asparagus = new Asparagus();
        blackOlives = new BlackOlives();
        eggplant = new Eggplant();
        garlic = new Garlic();
        mushroom = new Mushroom();
        onion = new Onion();
        redPepper = new RedPepper();
        spinach = new Spinach();
    }

    @Test
    public void veggieTest() {
        assertEquals(asparagus.toString(), "Asparagus");
        assertEquals(blackOlives.toString(), "Black Olives");
        assertEquals(eggplant.toString(), "Eggplant");
        assertEquals(garlic.toString(), "Garlic");
        assertEquals(mushroom.toString(), "Mushrooms");
        assertEquals(onion.toString(), "Onion");
        assertEquals(redPepper.toString(), "Red Pepper");
        assertEquals(spinach.toString(), "Spinach");
    }

}
