package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {

    private PizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void newYorkPizzaStoreTest() {
        Pizza pizza;

        pizza = newYorkPizzaStore.orderPizza("cheese");
        assertEquals(pizza.getName(), "New York Style Cheese Pizza");
        assertNotNull(pizza);
        assertTrue(pizza instanceof CheesePizza);

        pizza = newYorkPizzaStore.orderPizza("clam");
        assertEquals(pizza.getName(), "New York Style Clam Pizza");
        assertNotNull(pizza);
        assertTrue(pizza instanceof ClamPizza);

        pizza = newYorkPizzaStore.orderPizza("veggie");
        assertEquals(pizza.getName(), "New York Style Veggie Pizza");
        assertNotNull(pizza);
        assertTrue(pizza instanceof VeggiePizza);

    }
}
