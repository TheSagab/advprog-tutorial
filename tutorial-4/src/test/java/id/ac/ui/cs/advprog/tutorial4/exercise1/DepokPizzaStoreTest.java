package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {

    private PizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void depokPizzaStoreTest() {
        Pizza pizza;

        pizza = depokPizzaStore.orderPizza("cheese");
        assertEquals(pizza.getName(), "Depok Style Cheese Pizza");
        assertNotNull(pizza);
        assertTrue(pizza instanceof CheesePizza);

        pizza = depokPizzaStore.orderPizza("clam");
        assertEquals(pizza.getName(), "Depok Style Clam Pizza");
        assertNotNull(pizza);
        assertTrue(pizza instanceof ClamPizza);

        pizza = depokPizzaStore.orderPizza("veggie");
        assertEquals(pizza.getName(), "Depok Style Veggie Pizza");
        assertNotNull(pizza);
        assertTrue(pizza instanceof VeggiePizza);

    }
}
