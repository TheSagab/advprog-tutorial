package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SaltyClams;
import org.junit.Before;
import org.junit.Test;

public class ClamTest {

    private SaltyClams saltyClams;
    private FreshClams freshClams;
    private FrozenClams frozenClams;

    @Before
    public void setUp() {
        saltyClams = new SaltyClams();
        freshClams = new FreshClams();
        frozenClams = new FrozenClams();
    }

    @Test
    public void clamTest() {
        assertEquals(saltyClams.toString(), "Salty Clams from Java Sea");
        assertEquals(freshClams.toString(), "Fresh Clams from Long Island Sound");
        assertEquals(frozenClams.toString(), "Frozen Clams from Chesapeake Bay");
    }

}
