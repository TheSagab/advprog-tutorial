package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        /*for (Command command: commands) {
            command.execute();
        }*/

        commands.stream().forEach(Command::execute);
    }

    @Override
    public void undo() {
        // Reverse list so command executed backwards
        /*Collections.reverse(commands);

        for (Command command: commands) {
            command.undo();
        }

        Collections.reverse(commands);*/

        new LinkedList<>(commands).descendingIterator().forEachRemaining(Command::undo);

    }
}
