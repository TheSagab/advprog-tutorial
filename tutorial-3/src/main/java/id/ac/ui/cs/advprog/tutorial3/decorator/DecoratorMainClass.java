package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMainClass {
    public static void main(String[] args) {
        Food whopper = BreadProducer.THICK_BUN.createBreadToBeFilled();
        whopper = FillingDecorator.BEEF_MEAT.addFillingToBread(whopper);
        whopper = FillingDecorator.LETTUCE.addFillingToBread(whopper);
        whopper = FillingDecorator.TOMATO.addFillingToBread(whopper);
        whopper = FillingDecorator.CHILI_SAUCE.addFillingToBread(whopper);
        whopper = FillingDecorator.CUCUMBER.addFillingToBread(whopper);

        System.out.println(whopper.getDescription() + " costs " + whopper.cost());

    }
}
